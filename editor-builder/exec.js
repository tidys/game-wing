const {exec} = require('child_process')
module.exports = {
    exec(cmd, cb) {
        // return cb();
        let std = exec(cmd, (error, stdout, stderr) => {
            if (error) {
                console.log(error);
            }
        });
        std.stdout.on('data', (data) => {
            console.log(data);
        });
        std.stderr.on('error', (data) => {
            console.error(data);
            throw new Error(data);
        });
        std.on('exit', () => {
            console.log('runtime project build finished');
            cb && cb();
        });
        std.on('close', () => {
            console.log('close');
        });
    }
}
