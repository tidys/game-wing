const Path = require('path')
const FsExtra = require('fs-extra')
const CocosCreatorVersion = '2.4.6';
const CocosCreator = `/Applications/CocosCreator/Creator/${CocosCreatorVersion}/CocosCreator.app/Contents/MacOS/CocosCreator`;

if (!FsExtra.existsSync(CocosCreator)) {
    console.log(`找不到CocosCreator:${CocosCreator}`)
    return;
}
const Project = Path.join(__dirname, '../runtime')
if (!FsExtra.existsSync(Project)) {
    console.log(`找不到runtimeProject：${Project}`)
    return;
}

const Build = "platform=web-mobile;debug=true;md5Cache=false;"
const CmdPath = CocosCreatorVersion.startsWith('2.') ? 'path' : 'project' // 版本差异
const cmd = `${CocosCreator} --${CmdPath} ${Project} --build '${Build}'`
const {exec} = require('./exec')
exec(cmd, () => {
    console.log('build finished')
    // 将构建结果copy到编辑器的public中
    const EditorPublic = Path.join(__dirname, '../editor/public');
    const WebMobileDir = Path.join(__dirname, '../runtime/build/web-mobile')
    if (!FsExtra.existsSync(EditorPublic)) {
        return console.log(`目录不存在：${EditorPublic}`)
    }
    if (!FsExtra.existsSync(WebMobileDir)) {
        return console.log(`目录不存在：${WebMobileDir}`)
    }
    FsExtra.emptyDirSync(EditorPublic);
    FsExtra.copySync(WebMobileDir, EditorPublic)
    console.log('sync finished')
})

