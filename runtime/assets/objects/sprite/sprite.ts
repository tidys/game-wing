const comp = {
    info: {
        name: 'Sprite',
        icon: 'icon-sprite',
        desc: '一个带动画的对象，它是大多数项目的重要组成部分',
        type: 'sprite',
        group: '常规',
    }
}

function loadSignColor(cb) {
    cc.assetManager.loadAny(
        { uuid: '0275e94c-56a7-410f-bd1a-fc7483f7d14a' },
        (error, texture) => {
            cb(new cc.SpriteFrame(texture));
        })
}

export default (bundle: cc.AssetManager.Bundle, callback) => {
    let node = new cc.Node('sail-sprite');
    node.color = cc.Color.BLACK;
    let sprite = node.addComponent(cc.Sprite)
    sprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
    loadSignColor((spr) => {
        node.setContentSize(100, 100);
        sprite.spriteFrame = spr;
        callback(node);
    })
}
