const comp = {
    info: {
        name: 'Touch',
        icon: 'icon-touch',
        desc: '接受来自触摸屏设备的输入，以及访问设备方向和动作',
        type: 'touch',
        group: 'input',
    }
}
