const gameObject = {
    type: 'object',
    canInstantiate: false,
    info: {
        group: '常规',
        name: '系统',
        icon: 'icon-system',
        desc: '提供对engine的访问功能',
    },
    ace: {
        conditions: [
            {
                id: 'judge-bool',
                name: '判断实例布尔值',
                fn: () => {

                },
                group: '实例变量',
                desc: '判断实例对象的布尔值变量是否为真',
                asset: 'variable',
                params: [
                    {
                        name: '变量',
                        type: 'bool',
                    }
                ]
            },
            {
                id: 'frames',
                name: '每一帧',
                fn: () => {
                    console.log('每帧都执行');
                },
                group: '常规',
                desc: '每一帧都会执行',
            }
        ],
        actions: [
            {
                id: 'set-bool',
                name: '设置布尔值',
                fn: () => {

                },
                group: '变量操作',
                desc: '设置全局或者本地变量的数值',
            }
        ],
        expressions: []
    },
};
export default gameObject;
