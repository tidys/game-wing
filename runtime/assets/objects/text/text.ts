import Prop from './prop';

const gameObject = {
    type: 'object',
    info: {
        name: '文本',
        icon: 'icon-text',
        desc: '在屏幕中显示一些文本',
        group: '常规',
    },
    
    async create(bundle: cc.AssetManager.Bundle) {
        let node = new cc.Node('sail-label');
        node.color = cc.Color.BLACK;
        const label = node.addComponent(cc.Label);
        label.string = 'test-label';
        return node;
    },
    component: Prop,
};
export default gameObject;