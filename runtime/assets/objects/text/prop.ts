const { ccclass, property } = cc._decorator;

@ccclass
export default class Prop extends cc.Component {
    
    @property({ displayName: '透明度' })
    
    get opacity() {
        return this.node.opacity;
    }
    
    set opacity(v) {
        this.node.opacity = v;
    }
    
    @property({ displayName: '角度' })
    get angle() {
        return this.node.angle;
    }
    
    set angle(v) {
        this.node.angle = v;
    }
    
    start() {
    
    }
}