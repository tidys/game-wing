const { ccclass, property } = cc._decorator;

@ccclass
export default class FadeBehavior extends cc.Component {

    @property()
    fadeInTime = 0;

    @property({ tooltip: '启用后立即执行淡入淡出，或者等待开始的动作触发后执行', displayName: '启用' })
    fadeEnabled = true;

    start() {

    }
}

