import comp from './fadeBehavior'

const gameBehavior = {
    type: 'behavior',
    info: {
        name: '淡入淡出',
        icon: 'icon-fade',
        desc: '在一段时间内逐渐改变对象的不透明度，适用于让对象逐渐出现消失',
        group: '常规',
    },
    component: comp,
}
export default gameBehavior;
