export interface ComponentAttrs {
    name: string;
    default: any;
    displayName?: string,
    tooltip?: string,
    value: any;
    visible?: (() => boolean) | boolean;
}

export interface Info {
    icon?: string; // 显示的icon
    name: string; // 显示的名字
    desc: string;  // 组件描述
    group: string; // 组件的分组
}

export interface GameBehavior {
    type: string;
    bundle: string;
    info: Info;
    component: typeof cc.Component;
}

export interface OriginBehavior {
    bundle: string;
}
