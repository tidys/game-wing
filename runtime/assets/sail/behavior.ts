import sail from './index';
import { ComponentAttrs } from './declare';

export function collection(comps: Record<string, cc.Component>): Record<string, ComponentAttrs[]> {
    const behaviors: Record<string, ComponentAttrs[]> = {};
    for (let bundle in comps) {
        const comp = comps[bundle];
        const attrs = sail.getComponentAttrs(comp);
        attrs.forEach(attr => {
            attr.value = comp[attr.name];
        });
        behaviors[bundle] = attrs;
    }
    
    return behaviors;
}

