function defineComp (options) {
    return {};
}

const comp = {
    props: {
        angle: {
            name: '角度',
            tooltip: '',
            _v: 0,
            get value () {
                return this._v;
            },
            set value (v) {
                this._v = v;
                // 实现和creator同步
                comp.node.angle = v;
            },
        },
        zIndex: {
            _v: 0,
            get value () {
                return this._v;
            },
            set value (v) {
                this._v = v;
            }
        }
    },
    behavior: [],
    variable:[],
    node: {
        angle: 0,
    },
    create () {
        // 一堆的creator创建逻辑
        return this.node;
    },
    setup () {

    }
}
defineComp(comp);
comp.props.angle.value = 100;
console.log(comp.node.angle)
// const comp = new Comp();
// comp.property.angle.value = 1002;
// comp.property.zIndex.value = 9;
// console.log(comp.property.angle.value, comp.property.zIndex.value)
