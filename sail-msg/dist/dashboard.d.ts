export declare class List {
    name: string;
    uuid: string;
    path: string;
    constructor(name: string);
}
export declare const MSG: {
    List: string;
    Create: string;
    Delete: string;
    Update: string;
    Open: string;
    ShowDir: string;
};
