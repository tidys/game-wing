import {v4} from "uuid";

export class List {
    name: string = "";
    uuid: string = "";
    path: string = "";// 项目的路径

    constructor(name: string) {
        this.name = name;
        this.uuid = v4();
    }
}

export const MSG = {
    List: "dashboard-list",
    Create: "dashboard-create",
    Delete: "dashboard-delete",
    Update: "dashboard-update",
    Open: "dashboard-open",
    ShowDir: "dashboard-showDir"
};
