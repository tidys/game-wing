export class NetC2S {
    action: string;
    data: any;
}

export class NetS2C {
    action: string;
    data: any;
}
