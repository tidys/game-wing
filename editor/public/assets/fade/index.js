window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  fadeBehavior: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d4897so2o9G8pTqBqc/AkN/", "fadeBehavior");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var FadeBehavior = function(_super) {
      __extends(FadeBehavior, _super);
      function FadeBehavior() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.fadeInTime = 0;
        _this.fadeEnabled = true;
        return _this;
      }
      FadeBehavior.prototype.start = function() {};
      __decorate([ property() ], FadeBehavior.prototype, "fadeInTime", void 0);
      __decorate([ property({
        tooltip: "\u542f\u7528\u540e\u7acb\u5373\u6267\u884c\u6de1\u5165\u6de1\u51fa\uff0c\u6216\u8005\u7b49\u5f85\u5f00\u59cb\u7684\u52a8\u4f5c\u89e6\u53d1\u540e\u6267\u884c",
        displayName: "\u542f\u7528"
      }) ], FadeBehavior.prototype, "fadeEnabled", void 0);
      FadeBehavior = __decorate([ ccclass ], FadeBehavior);
      return FadeBehavior;
    }(cc.Component);
    exports.default = FadeBehavior;
    cc._RF.pop();
  }, {} ],
  fade: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "b9c79lqREpLqZ+KJWtWPnwa", "fade");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var fadeBehavior_1 = require("./fadeBehavior");
    var gameBehavior = {
      type: "behavior",
      info: {
        name: "\u6de1\u5165\u6de1\u51fa",
        icon: "icon-fade",
        desc: "\u5728\u4e00\u6bb5\u65f6\u95f4\u5185\u9010\u6e10\u6539\u53d8\u5bf9\u8c61\u7684\u4e0d\u900f\u660e\u5ea6\uff0c\u9002\u7528\u4e8e\u8ba9\u5bf9\u8c61\u9010\u6e10\u51fa\u73b0\u6d88\u5931",
        group: "\u5e38\u89c4"
      },
      component: fadeBehavior_1.default
    };
    exports.default = gameBehavior;
    cc._RF.pop();
  }, {
    "./fadeBehavior": "fadeBehavior"
  } ]
}, {}, [ "fade", "fadeBehavior" ]);
//# sourceMappingURL=index.js.map
