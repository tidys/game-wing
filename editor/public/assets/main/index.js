window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  MainScene: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1ad6a/EcY9Hwoc01+KBbjX9", "MainScene");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var NewClass = function(_super) {
      __extends(NewClass, _super);
      function NewClass() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      NewClass.prototype.start = function() {};
      NewClass = __decorate([ ccclass ], NewClass);
      return NewClass;
    }(cc.Component);
    exports.default = NewClass;
    cc._RF.pop();
  }, {} ],
  SailGameObject: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f2b1dqLCQROR7EUGi31++ZL", "SailGameObject");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var index_1 = require("./index");
    var behavior_1 = require("./behavior");
    var Variable = function() {
      function Variable() {}
      return Variable;
    }();
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SailGameObject = function(_super) {
      __extends(SailGameObject, _super);
      function SailGameObject() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.variables = [];
        _this.behaviors = [];
        return _this;
      }
      Object.defineProperty(SailGameObject.prototype, "x", {
        get: function() {
          return this.node.x;
        },
        set: function(v) {
          this.node.x = v;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(SailGameObject.prototype, "y", {
        get: function() {
          return this.node.y;
        },
        set: function(v) {
          this.node.y = v;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(SailGameObject.prototype, "width", {
        get: function() {
          return this.node.width;
        },
        set: function(v) {
          this.node.width = v;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(SailGameObject.prototype, "height", {
        get: function() {
          return this.node.height;
        },
        set: function(v) {
          this.node.height = v;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(SailGameObject.prototype, "zIndex", {
        get: function() {
          return this.node.zIndex;
        },
        set: function(v) {
          this.node.zIndex = v;
        },
        enumerable: false,
        configurable: true
      });
      SailGameObject.prototype.initVar = function(vars) {
        this.variables = [].concat(vars);
      };
      SailGameObject.prototype.init = function(data) {
        return __awaiter(this, void 0, void 0, function() {
          var dependencies, allBundles, i, bundle, data_1;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              dependencies = data.dependencies;
              allBundles = data.behaviors.concat(data.bundle);
              i = 0;
              _a.label = 1;

             case 1:
              if (!(i < allBundles.length)) return [ 3, 4 ];
              bundle = allBundles[i];
              data_1 = dependencies ? dependencies[bundle] : {};
              return [ 4, this.initBundle(bundle, data_1 || {}) ];

             case 2:
              _a.sent();
              _a.label = 3;

             case 3:
              i++;
              return [ 3, 1 ];

             case 4:
              return [ 2 ];
            }
          });
        });
      };
      SailGameObject.prototype.initBundle = function(bundleName, data) {
        return __awaiter(this, void 0, void 0, function() {
          var entry, node, script;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (!bundleName) {
                console.error("\u65e0\u6548\u7684bundle: " + bundleName);
                return [ 2 ];
              }
              entry = index_1.default.allBundles.find(function(el) {
                return el.bundle === bundleName;
              });
              if (!!entry) return [ 3, 2 ];
              return [ 4, index_1.default.loadBundle(bundleName) ];

             case 1:
              entry = _a.sent();
              _a.label = 2;

             case 2:
              node = this.node;
              if (node && node.isValid && entry && entry.component) {
                script = node.getComponent(entry.component);
                script || (script = node.addComponent(entry.component));
                index_1.default.setComponentProperties(script, data);
                return [ 2, script ];
              }
              return [ 2, null ];
            }
          });
        });
      };
      SailGameObject.prototype.addBehavior = function(bundleName, data) {
        return __awaiter(this, void 0, void 0, function() {
          var script;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this.initBundle(bundleName, data) ];

             case 1:
              script = _a.sent();
              script && (this.behaviors.find(function(el) {
                return el === bundleName;
              }) || this.behaviors.push(bundleName));
              return [ 2 ];
            }
          });
        });
      };
      SailGameObject.prototype.getBehaviorComponents = function() {
        return __awaiter(this, void 0, Promise, function() {
          var map, i, behavior, bundleInfo, script;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              map = {};
              i = 0;
              _a.label = 1;

             case 1:
              if (!(i < this.behaviors.length)) return [ 3, 4 ];
              behavior = this.behaviors[i];
              return [ 4, index_1.default.loadBundle(behavior) ];

             case 2:
              bundleInfo = _a.sent();
              if (bundleInfo) {
                script = this.node.getComponent(bundleInfo.component);
                script && (map[bundleInfo.bundle] = script);
              }
              _a.label = 3;

             case 3:
              i++;
              return [ 3, 1 ];

             case 4:
              return [ 2, map ];
            }
          });
        });
      };
      SailGameObject.prototype.removeBehavior = function(bundleName) {
        return __awaiter(this, void 0, void 0, function() {
          var ret, item, comp, idx;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this.getBehaviorComponents() ];

             case 1:
              ret = _a.sent();
              for (item in ret) {
                comp = ret[item];
                if (item === bundleName) {
                  this.node.removeComponent(comp);
                  idx = this.behaviors.findIndex(function(el) {
                    return el === bundleName;
                  });
                  this.behaviors.splice(idx, 1);
                  return [ 2, true ];
                }
              }
              return [ 2, false ];
            }
          });
        });
      };
      SailGameObject.prototype.modifyVariable = function(id, value) {
        var ret = this.variables.find(function(el) {
          return el.id === id;
        });
        ret ? ret.value = value : this.variables.push({
          id: id,
          value: value
        });
      };
      SailGameObject.prototype.dumpBehaviorData = function() {
        return __awaiter(this, void 0, Promise, function() {
          var ret;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this.getBehaviorComponents() ];

             case 1:
              ret = _a.sent();
              return [ 2, behavior_1.collection(ret) ];
            }
          });
        });
      };
      SailGameObject.prototype.setProperty = function(bundle, key, value) {
        return __awaiter(this, void 0, void 0, function() {
          var bundleInfo, script;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, index_1.default.loadBundle(bundle) ];

             case 1:
              bundleInfo = _a.sent();
              if (bundleInfo) {
                script = this.node.getComponent(bundleInfo.component);
                script && index_1.default.hasProperty(script, key) ? script[key] = value : cc.log("\u672a\u77e5\u7684\u5c5e\u6027\uff1a", key);
              }
              return [ 2 ];
            }
          });
        });
      };
      __decorate([ property() ], SailGameObject.prototype, "bundle", void 0);
      __decorate([ property() ], SailGameObject.prototype, "originObjectID", void 0);
      __decorate([ property({
        type: [ Variable ]
      }) ], SailGameObject.prototype, "variables", void 0);
      __decorate([ property() ], SailGameObject.prototype, "behaviors", void 0);
      __decorate([ property() ], SailGameObject.prototype, "x", null);
      __decorate([ property() ], SailGameObject.prototype, "y", null);
      __decorate([ property() ], SailGameObject.prototype, "width", null);
      __decorate([ property() ], SailGameObject.prototype, "height", null);
      __decorate([ property() ], SailGameObject.prototype, "zIndex", null);
      SailGameObject = __decorate([ ccclass ], SailGameObject);
      return SailGameObject;
    }(cc.Component);
    exports.default = SailGameObject;
    cc._RF.pop();
  }, {
    "./behavior": "behavior",
    "./index": "index"
  } ],
  behavior: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "290f9Z/J4lK6ZAPE8oTZ6YL", "behavior");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.collection = void 0;
    var index_1 = require("./index");
    function collection(comps) {
      var behaviors = {};
      var _loop_1 = function(bundle) {
        var comp = comps[bundle];
        var attrs = index_1.default.getComponentAttrs(comp);
        attrs.forEach(function(attr) {
          attr.value = comp[attr.name];
        });
        behaviors[bundle] = attrs;
      };
      for (var bundle in comps) _loop_1(bundle);
      return behaviors;
    }
    exports.collection = collection;
    cc._RF.pop();
  }, {
    "./index": "index"
  } ],
  declare: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a1ecaTc5mlAWK6KLTwy9ldi", "declare");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    cc._RF.pop();
  }, {} ],
  index: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "709dfCOcO5Ks5i2Cw+Dla7/", "index");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var SailGameObject_1 = require("./SailGameObject");
    var DefaultBundle = {
      bundle: "",
      info: {
        name: "",
        icon: "",
        desc: "",
        group: "default"
      },
      create: function() {
        return null;
      },
      props: function() {
        return {};
      }
    };
    var VarType;
    (function(VarType) {
      VarType["String"] = "string";
      VarType["Boolean"] = "boolean";
      VarType["Number"] = "number";
    })(VarType || (VarType = {}));
    var GameBehavior = "behavior";
    var GameObject = "object";
    var Sail = function() {
      function Sail() {
        this.VarType = VarType;
        this.allBundles = [];
      }
      Sail.prototype.getComponentAttrs = function(comp) {
        var ret = {};
        var attrs = cc.Class.Attr.getClassAttrs(comp);
        Object.keys(attrs).forEach(function(key) {
          var values = key.split(cc.Class.Attr.DELIMETER);
          if (2 === values.length) {
            var compProperty = values[0], prop = values[1];
            ret.hasOwnProperty(compProperty) || (ret[compProperty] = {});
            ret[compProperty][prop] = attrs[key];
          }
        });
        var result = [];
        Object.keys(ret).map(function(key) {
          ret[key].name = key;
          result.push(ret[key]);
        });
        return result;
      };
      Sail.prototype.loadGameBehavior = function(entry) {
        return __awaiter(this, void 0, void 0, function() {
          var component;
          return __generator(this, function(_a) {
            component = entry.component;
            component && "function" === typeof component;
            return [ 2, entry ];
          });
        });
      };
      Sail.prototype.loadGameObject = function(entry, bundleData) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            entry = Object.assign({}, DefaultBundle, entry);
            return [ 2, entry ];
          });
        });
      };
      Sail.prototype.loadBundle = function(name) {
        return __awaiter(this, void 0, void 0, function() {
          var bundleData, bundle, entry, type, ret;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this.downloadBundle(name) ];

             case 1:
              bundleData = _a.sent();
              bundle = require(name);
              if (bundle) {
                entry = bundle.default || bundle;
                if ("object" === typeof entry) {
                  entry.bundle = bundleData.name;
                  type = entry.type;
                  ret = null;
                  type === GameBehavior ? ret = this.loadGameBehavior(entry) : type === GameObject && (ret = this.loadGameObject(entry, bundleData));
                  ret && this.allBundles.push(ret);
                  return [ 2, ret ];
                }
              }
              return [ 2, null ];
            }
          });
        });
      };
      Sail.prototype.downloadBundle = function(name) {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(resolve, reject) {
              cc.assetManager.loadBundle(name, function(err, bundle) {
                resolve(bundle);
              });
            }) ];
          });
        });
      };
      Sail.prototype.createGameObject = function(originObjectID, data) {
        return __awaiter(this, void 0, Promise, function() {
          var gameObject, node, objectScript;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this.loadBundle(data.bundle) ];

             case 1:
              gameObject = _a.sent();
              if (!(gameObject && gameObject.create && "object" === typeof gameObject)) return [ 3, 5 ];
              return [ 4, gameObject.create() ];

             case 2:
              node = _a.sent();
              if (!node) return [ 3, 5 ];
              objectScript = node.addComponent(SailGameObject_1.default);
              objectScript.originObjectID = originObjectID;
              objectScript.bundle = gameObject.bundle;
              if (!data) return [ 3, 4 ];
              this.setComponentProperties(objectScript, data);
              return [ 4, objectScript.init(data) ];

             case 3:
              _a.sent();
              _a.label = 4;

             case 4:
              return [ 2, node ];

             case 5:
              return [ 2, null ];
            }
          });
        });
      };
      Sail.prototype.setComponentProperties = function(script, data) {
        var _this = this;
        if (data && "object" === typeof data && !Array.isArray(data)) {
          var keys = Object.keys(data);
          keys.forEach(function(key) {
            _this.hasProperty(script, key) ? script[key] = data[key] : console.log("key:", key);
          });
        }
      };
      Sail.prototype.serializeScene = function(node) {
        return __awaiter(this, void 0, void 0, function() {
          var ret, i, child, script, item, dependencies, allBundles, i_1, bundle, info;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              ret = [];
              if (!node) return [ 3, 7 ];
              i = 0;
              _a.label = 1;

             case 1:
              if (!(i < node.children.length)) return [ 3, 7 ];
              child = node.children[i];
              script = child.getComponent(SailGameObject_1.default);
              if (!script) return [ 3, 6 ];
              item = this._serializeComponent(child, script.constructor);
              ret.push(item);
              dependencies = {};
              allBundles = script.behaviors.concat(item.bundle);
              i_1 = 0;
              _a.label = 2;

             case 2:
              if (!(i_1 < allBundles.length)) return [ 3, 5 ];
              bundle = allBundles[i_1];
              return [ 4, this.loadBundle(bundle) ];

             case 3:
              info = _a.sent();
              info && info.component && (dependencies[bundle] = this._serializeComponent(child, info.component));
              _a.label = 4;

             case 4:
              i_1++;
              return [ 3, 2 ];

             case 5:
              item["dependencies"] = dependencies;
              _a.label = 6;

             case 6:
              i++;
              return [ 3, 1 ];

             case 7:
              return [ 2, ret ];
            }
          });
        });
      };
      Sail.prototype._serializeComponent = function(node, component) {
        var compData = {};
        var script = node.getComponent(component);
        if (script) {
          var attrs = this.getComponentAttrs(component);
          attrs.forEach(function(attr) {
            compData[attr.name] = script[attr.name];
          });
        }
        return compData;
      };
      Sail.prototype.hasProperty = function(obj, key) {
        var b = obj.hasOwnProperty(key);
        b || (b = key in obj);
        if (!b) {
          var proto = obj;
          do {
            b = !!Object.getOwnPropertyDescriptor(proto, key);
            proto = Object.getPrototypeOf(proto);
          } while (!b && proto);
        }
        return b;
      };
      return Sail;
    }();
    var sail = new Sail();
    window.sail = sail;
    exports.default = sail;
    cc._RF.pop();
  }, {
    "./SailGameObject": "SailGameObject"
  } ],
  touch: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f4be8pg22JNxJTso+xu/OhS", "touch");
    var comp = {
      info: {
        name: "Touch",
        icon: "icon-touch",
        desc: "\u63a5\u53d7\u6765\u81ea\u89e6\u6478\u5c4f\u8bbe\u5907\u7684\u8f93\u5165\uff0c\u4ee5\u53ca\u8bbf\u95ee\u8bbe\u5907\u65b9\u5411\u548c\u52a8\u4f5c",
        type: "touch",
        group: "input"
      }
    };
    cc._RF.pop();
  }, {} ]
}, {}, [ "MainScene", "touch", "SailGameObject", "behavior", "declare", "index" ]);
//# sourceMappingURL=index.js.map
