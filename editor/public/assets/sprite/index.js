window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  sprite: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "df4f9d2MLVPtq198hHfHktK", "sprite");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var comp = {
      info: {
        name: "Sprite",
        icon: "icon-sprite",
        desc: "\u4e00\u4e2a\u5e26\u52a8\u753b\u7684\u5bf9\u8c61\uff0c\u5b83\u662f\u5927\u591a\u6570\u9879\u76ee\u7684\u91cd\u8981\u7ec4\u6210\u90e8\u5206",
        type: "sprite",
        group: "\u5e38\u89c4"
      }
    };
    function loadSignColor(cb) {
      cc.assetManager.loadAny({
        uuid: "0275e94c-56a7-410f-bd1a-fc7483f7d14a"
      }, function(error, texture) {
        cb(new cc.SpriteFrame(texture));
      });
    }
    exports.default = function(bundle, callback) {
      var node = new cc.Node("sail-sprite");
      node.color = cc.Color.BLACK;
      var sprite = node.addComponent(cc.Sprite);
      sprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
      loadSignColor(function(spr) {
        node.setContentSize(100, 100);
        sprite.spriteFrame = spr;
        callback(node);
      });
    };
    cc._RF.pop();
  }, {} ]
}, {}, [ "sprite" ]);
//# sourceMappingURL=index.js.map
