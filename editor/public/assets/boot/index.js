window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  index: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "709dfCOcO5Ks5i2Cw+Dla7/", "index");
    var Sail = function() {
      function Sail() {
        this.name = "boot";
      }
      Sail.prototype.loadObject = function(name, callback) {
        cc.assetManager.loadBundle(name, function(error, bundle) {
          var object = require(name);
          if (object) {
            var entry = object.default || object;
            "function" === typeof entry && entry(bundle, callback);
          }
        });
      };
      return Sail;
    }();
    var sail = new Sail();
    window.sail = sail;
    cc._RF.pop();
  }, {} ]
}, {}, [ "index" ]);