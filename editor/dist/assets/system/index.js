window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  system: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "7eb8c9kXsVCBa5uORhlqGDE", "system");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var gameObject = {
      type: "object",
      canInstantiate: false,
      info: {
        group: "\u5e38\u89c4",
        name: "\u7cfb\u7edf",
        icon: "icon-system",
        desc: "\u63d0\u4f9b\u5bf9engine\u7684\u8bbf\u95ee\u529f\u80fd"
      },
      ace: {
        conditions: [ {
          id: "judge-bool",
          name: "\u5224\u65ad\u5b9e\u4f8b\u5e03\u5c14\u503c",
          fn: function() {},
          group: "\u5b9e\u4f8b\u53d8\u91cf",
          desc: "\u5224\u65ad\u5b9e\u4f8b\u5bf9\u8c61\u7684\u5e03\u5c14\u503c\u53d8\u91cf\u662f\u5426\u4e3a\u771f",
          asset: "variable",
          params: [ {
            name: "\u53d8\u91cf",
            type: "bool"
          } ]
        }, {
          id: "frames",
          name: "\u6bcf\u4e00\u5e27",
          fn: function() {
            console.log("\u6bcf\u5e27\u90fd\u6267\u884c");
          },
          group: "\u5e38\u89c4",
          desc: "\u6bcf\u4e00\u5e27\u90fd\u4f1a\u6267\u884c"
        } ],
        actions: [ {
          id: "set-bool",
          name: "\u8bbe\u7f6e\u5e03\u5c14\u503c",
          fn: function() {},
          group: "\u53d8\u91cf\u64cd\u4f5c",
          desc: "\u8bbe\u7f6e\u5168\u5c40\u6216\u8005\u672c\u5730\u53d8\u91cf\u7684\u6570\u503c"
        } ],
        expressions: []
      }
    };
    exports.default = gameObject;
    cc._RF.pop();
  }, {} ]
}, {}, [ "system" ]);
//# sourceMappingURL=index.js.map
