const production = process.env.NODE_ENV === 'production';
module.exports = {
    pwa: {
        name: 'sail',
        iconPaths: {
            favicon16: './img/icons/favicon-16x16.png',
            favicon32: './img/icons/favicon-32x32.png',
        },
    },
    publicPath: './',
    // devServer: {
    //     host: 'sail.com',
    //     port: 80,
    //     disableHostCheck: true,
    // },

    configureWebpack () {
        const devtool = production ? 'source-map' : 'source-map';
        return {
            devtool,
        };
    },
};