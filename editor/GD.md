```json5
{
  gdVersion: {
    // 版本信息
  },
  properties: {
    // 属性设置
  },
  resources: [],
  layouts: [
    {
      name: 'scenes',
      instances: [
        {
          initialVar: [
            {
              name: 'v1',
              value: 2,
            }
          ]
        }
      ],
      objects: [
        {
          type: "text",
          name: 't1',
          variables: [
            {
              name: 'v1',
              value: 2,
            }
          ]
        }
      ]
    }
  ]
}
```

sail

```json5
{
  id: 'scene',
  data: [
    {
      type: 'text',
      name: 't1'
    }
  ]
}
```