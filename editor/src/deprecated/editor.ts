interface List {
    name: string;
    cb: Function | null;
}

class SailEditor {
    _event: Record<string, Array<Function | null>> = {};
    Scene = null;

    on(event: string, cb: Function | null) {
        if (!Object.prototype.hasOwnProperty.call(this._event, event)) {
            this._event[event] = [];
        }
        this._event[event].push(cb);
    }

    emit(event: string) {
        if (Object.prototype.hasOwnProperty.call(this._event, event) && Array.isArray(this._event[event])) {
            this._event[event].forEach(cb => {
                cb && cb();
            });
        }
    }
}

(window as any).Editor = new SailEditor();
