import install from '../utils/install';
import menu from './menu.vue';
import isNumber = cc.js.isNumber;
import { Emitter } from '@/ui-kit/index';

export class IUiMenuItem {
    name = '';
    enabled? = true;
    callback: Function | null = null;
}

export const Msg = {
    ShowMenu: 'show-menu',
};

export interface MenuOptions {
    x: number;
    y: number;
}

export function showMenuByMouseEvent(event: MouseEvent, newMenus: IUiMenuItem[]): void {
    const options: MenuOptions = {
        x: event.clientX + 2,
        y: Math.abs(event.clientY),
    };
    Emitter.emit(Msg.ShowMenu, options, newMenus || []);
}

export default install(menu);
