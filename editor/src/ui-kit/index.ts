import { App, Component } from 'vue';
import Button from './button/index';
import panel from './panel/index';
import prop from './prop/index';
import tree from './tree';
import menu from './menu';
import window from './window';
import dialog from './dialog/index';
import { TinyEmitter } from 'tiny-emitter';

const components: Component[] = [Button, panel, prop, tree, menu, window, dialog];
const install = (app: App): void => {
    components.forEach((key) => {
        app.component(key.name || '', key);
    });
};

export const Emitter = new TinyEmitter();
export default { install };

