export interface HeadData {
    type?: 'bool' | 'string';
    label: string;
    key: string;
}
