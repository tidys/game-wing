import { generate } from 'shortid';
import install from '../utils/install';
import window from './window.vue';

export class UiWindowOptions {
    id? = generate();
    width? = 400;
    height? = 400;
    resize?: boolean;
    title? = '';
    closeCB?: Function | null;
    responseCB?: Function | null;
}

export const ProvideKey = {
    CloseWindow: 'CloseWindow',
    ResponseCB: 'ResponseCB',
};

export default install(window);
