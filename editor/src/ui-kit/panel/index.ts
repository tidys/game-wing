import install from '../utils/install';
import panel from './panel.vue';

export default install(panel);
