import { UiWindowOptions } from '../window/index';
import install from '../utils/install';
import dialog from './dialog.vue';

export const Msg = {
    ShowDialog: 'show-dialog',
};

export class DialogOptions extends UiWindowOptions {
    comp?: any;// DefineComponent | null,// 组件实例
    data?: any;// 传递给组件的数据
    clickOutsideClose?: boolean;
}

export default install(dialog);
