export interface TabItem {
    id: string;
    name: string;
    canClose?: boolean;
    selected?: boolean;
}

export const ProvideKey = {
    CloseTab: 'close-tab',
};
