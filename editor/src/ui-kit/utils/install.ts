import { App, Plugin, DefineComponent } from 'vue';

export type InstallType<T> = T & Plugin;
export const install = <T>(comp: T, customName?: string): T & Plugin => {
    const c = comp as any;
    c.install = (app: App, name?: string) => {
        const defaultName = c.name || '';
        app.component(customName || name || defaultName, comp);
    };
    return c as T & Plugin;
};
export default install;
