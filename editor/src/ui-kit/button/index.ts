import install from '../utils/install';
import button from './button.vue';

export default install(button);
