import install from '../utils/install';
import tree from './tree.vue';
import { generate } from 'shortid';

export class TreeData {
    id: string = generate();
    name = '';
    icon?: string;
    contextmenu: ((event: any, data: TreeData) => void) | null = null;
    dblclick: ((event: any, data: TreeData) => void) | null = null;
    click: ((event: any, data: TreeData) => void) | null = null;
    change: ((event: any, data: TreeData) => boolean) | null = null;
    children: TreeData[] = [];
    data: any = null;
    
    constructor(opts: {
        id?: string,
        name: string,
        ctxMenu?: ((event: any) => void) | null
    }) {
        const { id, name, ctxMenu } = opts;
        this.id = id || this.id;
        this.name = name;
        this.contextmenu = ctxMenu || null;
    }
}

export const ProvideKey = {
    CanDraggable: 'CanDraggable',
    DragStart: 'DragStart',
    DoubleClick: 'double-click',
    ContextMenu: 'context-menu',
};
export const Msg = {
    RenameItem: 'rename-item',
};
export default install(tree);
