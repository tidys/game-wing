import { createRouter, createWebHistory, RouteRecordRaw, createWebHashHistory } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'home',
        component: () => import( '../views/editor/index.vue'),
        meta: {
            title: 'sail editor',
        },
    },
    {
        path: '/mall',
        name: 'mall',
        component: () => import('../mall/index.vue'),
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import('../views/dashboard/index.vue'),
    },
    {
        path: '/editor',
        name: 'editor',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import( '../views/editor/index.vue'),
        props: (route) => {
            const { projectID } = route.query;
            return {
                projectID,
            };
        },
        
    },
];

const router = createRouter({
    history: createWebHashHistory(process.env.BASE_URL),
    routes,
});

export default router;
