declare namespace sail {
    export interface flag {
        bundle: string;
        type: string;
    }
    
    export interface Prop {
        name: string;
        value: any;
        tooltip: string;
        visible: boolean;
    }
    
    export interface Variable {
        id: string;// 唯一ID
        name: string,
        desc: string,
        value: string | boolean | number,
        type: string,
    }
    
    export enum VarType {
        String = 'string',
        Boolean = 'boolean',
        Number = 'number',
    }
    
    export interface Info {
        icon?: string; // 显示的icon
        name: string; // 显示的名字
        desc: string;  // 组件描述
        group: string; // 组件的分组
    }
    
    export interface GameBehavior {
        type: string;
        bundle: string;
        info: Info;
        component: typeof cc.Component;
    }
    
    export interface ComponentAttr {
        name: string;
        default: any;
        displayName?: string,
        value?: any;
        tooltip?: string,
        visible?: (() => boolean) | boolean;
    }
    
    export interface ACEParam {
        name: string;
        type: string;
    }
    
    export interface ConditionData {
        id: string,
        name: string,
        fn: (params: any) => void,
        group: string,
        desc: string,
        icon?: string,
        params?: ACEParam[];
        
    }
    
    export interface GameObject {
        type: string;
        bundle: string;
        canInstantiate?: boolean;// 是否可以实例化，system插件无法多实例
        info: Info;
        component: typeof cc.Component;
        
        create(): void;
        
        ace?: {
            conditions?: ConditionData[];
            actions?: ConditionData[];
            expressions?: ConditionData[];
        };
    }
    
    // 对应的是组件脚本
    export interface SailGameObject {
        originObjectID: string;
        bundle: string;
        variables?: Variable[];
        behaviors: string[];
        dependencies: Record<string, Prop>;
    }
    
    export interface SerializeData {
        bundle: string;
        originObjectID: string;
        x: string;
        y: string;
        width: string;
        height: string;
        zIndex: number;
    }
    
    export async function serializeScene(node: cc.Node);
    
    export function getComponentAttrs(comp: typeof cc.Component): ComponentAttr[];
    
    export async function loadBundle(name: string): Promise<GameObject | GameBehavior>;
    
    export async function downloadBundle(name: string): Promise<cc.AssetManager.Bundle>;
    
    export function createGameObject(originObjectID: string, data: SailGameObject): Promise<cc.Node | null>;
    
    export function initGameObject(node: cc.Node, opts: any);
    
    export function setProperty(nodeUUID: string, key: string, value: any);
}