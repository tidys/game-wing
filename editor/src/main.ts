import { createApp } from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import ui from './ui-kit/index';
import './assets/icon/iconfont.css';
import { createPinia } from 'pinia';

createApp(App)
    .use(ui)
    .use(createPinia())
    .use(router)
    .mount('#app');
