import axios from 'axios';

class Server {
    private url = 'http://localhost:2021';

    async connect() {

    }

    public async sendProjectMsg(msg: string, data: any = null) {
        return await this._send('project', msg, data);
    }

    public async sendDashboardMsg(msg: string, data: any = null) {
        return await this._send('dashboard', msg, data);
    }

    private async _send(target: string, msg: string, data: any = null) {
        const ret = await axios({
            method: 'post',
            url: `${this.url}/${target}`,
            data: {
                action: msg,
                data: data,
            },
        });
        return ret.data;
    }
}

const server = new Server();
export default server;
