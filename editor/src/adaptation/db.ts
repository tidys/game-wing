import { ProjectList } from '../views/dashboard/store/tyeps';

export class DB {
    projects: ProjectList[] = [];

    addProject(name?: string) {
        name = name || (new Date().getTime()
            .toFixed(4));
        this.projects.push({
            id: '111',
            name,
        });
    }
}

export const db = new DB();
