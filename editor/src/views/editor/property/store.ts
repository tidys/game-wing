import { defineStore } from 'pinia';

export interface PropertyState {
    id: string;
    originObjectID: string;// 对象的ID
    bundle: string;// 使用的bundle
    variables: sail.Variable[];
    behaviors: Record<string, sail.ComponentAttr[]>;
    props: sail.ComponentAttr[];
}

const propertyStore = defineStore('property-node', {
    state: (): PropertyState => {
        return {
            id: '',
            bundle: '',
            originObjectID: '',
            variables: [],
            behaviors: {},
            props: [],
        };
    },
    actions: {
        updateProperty(state: PropertyState) {
            this.id = state.id;
            this.bundle = state.bundle;
            this.originObjectID = state.originObjectID;
            this.variables = state.variables;
            this.behaviors = state.behaviors;
            this.props = state.props;
        },
        reset() {
            this.id = '';
            this.bundle = '';
            this.originObjectID = '';
            this.variables = [];
            this.behaviors = {};
            this.props = [];
        },
    },
});
export default propertyStore;