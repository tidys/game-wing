import pluginsStore from '../../store/plugins';
import { IAssetInfo } from '../../store/types';
import assetsStore from '../../store/assets';
function getTarget(id:string) {
    if (id) {
        // 因为system的原因，先从plugin里面查找，后续需要统一下这块的流程
        const pluginData: sail.GameObject | null = pluginsStore().getObjectByID(id);
        if (pluginData && pluginData.info) {
            return pluginData.info;
        }
        const info: IAssetInfo | null = assetsStore().getAsset(id);
        if (info) {
            return info;
        }
    }
    return null;
}
export function getTargetName(id: string) {
    const ret = getTarget(id);
    if (ret && ret.name !== undefined) {
        return ret.name;
    }
    return 'unknow';
}
export function getTargetIcon(id:string) {
    const ret = getTarget(id);
    if (ret && ret.icon !== undefined) {
        return ret.icon;
    }
    return 'icon-unknow';
}
