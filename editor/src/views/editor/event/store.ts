import { defineStore } from 'pinia';
import { EventSheet } from '../store/events';

export interface ISheet {
    sheet: EventSheet | null,
}

const sheetStore = defineStore('sheet-item', {
    state: (): ISheet => {
        return {
            sheet: null,
        };
    },
    getters: {},
    actions: {},
});
export default sheetStore;
