import { showActions, showConditions, showConditionDependObject, showParams } from '../scene/inquirer';
import { ListItem } from '../scene/inquirer/common-list';
import eventsStore from '../store/events';
import pluginsStore from '../store/plugins';

export function inquirerNewEventBlock(sheetID?: string, blockID?: string): void {
    if (sheetID) {
        showConditionDependObject((data: ListItem) => {
            // 选择目标
            const useBundle = data.data;
            // 获取目标身上可用的条件
            showConditions(useBundle, [], (useCondition: string) => {
                const [bundleID, conditionID] = useCondition.split(':');
                // 选择条件
                const ret: sail.ConditionData | null = pluginsStore().getConditionByID(bundleID, conditionID);
                if (ret?.params?.length) {
                    // 判断是否有参数，打开参数界面
                    showParams(ret.params, (data: any) => {
                    
                    });
                } else {
                    if (!blockID) {
                        const block = eventsStore().addBlock(sheetID);
                        if (block) {
                            blockID = block.id;
                        }
                    }
                    if (blockID) {
                        eventsStore().addBlockCondition(
                            sheetID,
                            blockID,
                            {
                                bundleOrObjID: useBundle,
                                condition: useCondition,
                            },
                        );
                    }
                }
            });
        });
    }
}

export function inquirerNewEventAction(sheetID: string, blockID: string) {
    showConditionDependObject((obj: ListItem) => {
        const useBundle = obj.data;
        showActions(useBundle, [], (action: ListItem) => {
            eventsStore().addBlockAction(sheetID, blockID, {
                target: useBundle,
                action: action.data,
            });
        });
    });
}
