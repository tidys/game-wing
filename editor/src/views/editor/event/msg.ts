export const Msg = {
    OnInitEvent: 'OnInitEvent',
};
export const ProvideKey = {
    AddComment: 'AddComment',
    RemoveComment: 'RemoveComment',
    ChangeComment: 'ChangeComment',
    AddBlock: 'AddBlock',
    RemoveBlock: 'RemoveBlock',
    AddBlockCondition: 'AddBlockCondition',
    RemoveBlockCondition: 'RemoveBlockCondition',
    AddBlockAction: 'AddBlockAction',
    RemoveBlockAction: 'RemoveBlockAction',
};
