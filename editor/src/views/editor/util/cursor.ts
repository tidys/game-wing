export function setCursor(cursor = 'default'):void {
    const canvasElement: HTMLCanvasElement = cc.game.canvas as HTMLCanvasElement;
    if (canvasElement) {
        canvasElement.style.cursor = cursor;
    }
}
