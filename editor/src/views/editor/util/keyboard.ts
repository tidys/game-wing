// event.buttons
// 0  : 没有按键或者是没有初始化
// 1  : 鼠标左键
// 2  : 鼠标右键
// 4  : 鼠标滚轮或者是中键
// 8  : 第四按键 (通常是“浏览器后退”按键)
// 16 : 第五按键 (通常是“浏览器前进”)


// event.button
// 0：主按键，通常指鼠标左键或默认值
// 1：辅助按键，通常指鼠标滚轮中键
// 2：次按键，通常指鼠标右键
// 3：第四个按钮，通常指浏览器后退按钮
// 4：第五个按钮，通常指浏览器的前进按钮
export function isMouseLeft(event: MouseEvent): boolean {
    return event.buttons === 1 || event.button === 0;
}

export function isMouseRight(event: MouseEvent): boolean {
    return event.buttons === 2 || event.button === 2;
}

export function isPressShift(event: KeyboardEvent): boolean {
    return event.shiftKey || event.keyCode === 16;
}

export function isPressCtrl(event: MouseEvent): boolean {
    return event.metaKey || event.ctrlKey;
}

export function isPressSpace(event: KeyboardEvent): boolean {
    return event.keyCode === 32;
}
