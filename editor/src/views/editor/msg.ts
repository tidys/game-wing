export const Msg = {
    CreateNewObject: 'CreateNewObject',
    OpenAsset: 'OpenAsset',
    CloseAsset: 'CloseAsset',
    ShowAssetProperty: 'ShowAssetProperty',
    ShowNodeProperty: 'ShowNodeProperty',
    RenderScene: 'RenderScene',
    RemoveEventAsset: 'RemoveEventAsset',
    RemoveSceneAsset: 'RemoveSceneAsset',
    RemoveObjectAsset: 'RemoveObjectAsset',
    AssetRename: 'asset-rename',
};
