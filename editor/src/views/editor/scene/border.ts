// 游戏框
import { sceneAssistant } from './index';

export class Border {
    private graphics: cc.Graphics | null = null;

    init():cc.Node {
        const node = new cc.Node('board');
        this.graphics = node.addComponent(cc.Graphics);
        this._draw(this.graphics);
        return node;
    }

    _getGameSize():{width:number, height:number} {
        const width = 960;
        const height = 640;
        return {width, height};
    }

    private _draw(graphics: cc.Graphics | null) {
        if (graphics) {
            const {width, height} = this._getGameSize();
            graphics.clear(true);
            graphics.strokeColor = cc.Color.BLUE;
            graphics.rect(-width / 2, -height / 2, width, height);
            graphics.lineWidth = 3 / sceneAssistant.scale;
            graphics.stroke();
        }
    }

    update():void {
        this._draw(this.graphics);
    }
}
