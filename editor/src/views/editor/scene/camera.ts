import { createSingleSprite } from './util';
import { sceneAssistant } from './index';

export class Camera {
    private cameraNode: cc.Node | null = null;

    init(callback: Function): cc.Node {
        const cameraNode = new cc.Node('Sail-Camera');
        // createSingleSprite(cameraNode);
        cameraNode.setSiblingIndex(0);
        const camera = cameraNode.addComponent(cc.Camera);
        const ClearFlags = cc.Camera.ClearFlags;
        camera.clearFlags = ClearFlags.COLOR | ClearFlags.DEPTH | ClearFlags.STENCIL;
        camera.backgroundColor = cc.Color.WHITE;
        camera.depth = -1;
        this.cameraNode = cameraNode;
        cameraNode.on(cc.Node.EventType.MOUSE_DOWN, (event: cc.Event.EventTouch) => {
            callback && callback(event);
        });
        this.update();
        return cameraNode;
    }

    update(): void {
        const scale = sceneAssistant.scale;
        const { width, height } = cc.Canvas.instance.designResolution;
        this.cameraNode?.setContentSize(width / scale, height / scale);
    }
}
