export function loadScript(url: string): Promise<void> {
    return new Promise((resolve) => {
        const script = document.createElement('script');
        script.src = url;
        document.body.appendChild(script);
        script.addEventListener(
            'load',
            function () {
                document.body.removeChild(script);
                resolve();
            },
            false,
        );
    });
}

export function createSingleSprite(parent: cc.Node): void {
    const uuid = '0275e94c-56a7-410f-bd1a-fc7483f7d14a';
    cc.assetManager.loadAny({ uuid }, (error, texture) => {
        const node: cc.Node = new cc.Node('sprite');
        node.opacity = 180;
        node.color = cc.Color.GRAY;
        const sprite: cc.Sprite = node.addComponent(cc.Sprite);
        sprite.spriteFrame = new cc.SpriteFrame(texture);
        sprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
        const widget = node.addComponent(cc.Widget);
        widget.alignMode = cc.Widget.AlignMode.ALWAYS;
        widget.isAlignRight = true;
        widget.isAlignLeft = true;
        widget.isAlignTop = true;
        widget.isAlignBottom = true;
        widget.left = widget.right = widget.top = widget.bottom = 0;
        node.parent = parent;
    });
}
