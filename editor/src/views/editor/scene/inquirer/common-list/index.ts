export interface ListGroup {
    title: string;
    data: ListItem[];
}

export interface ListItem {
    data: string; // bundle的名字
    icon?: string;
    label: string;
    desc: string;
    group: string;// 分组信息
}

export const ProvideKey = {
    OnSelect: 'common-list-on-select',
};
