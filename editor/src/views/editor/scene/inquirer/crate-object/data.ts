import { sceneAssistant } from '../../index';
import assetsStore from '../../../store/assets';
import { IAssetObject } from '../../../store/types';
import { ListItem } from '../common-list';
import { toRaw } from 'vue';
import { cloneDeep } from 'lodash';

function choosePosition(cb: Function): void {
    // 后续再实现吧，现在直接添加到(0,0)
}

interface InitGameObjectOpts {
    save: boolean;
    select: boolean;
}

// 根据对象ID，实例化
export async function instanceGameObject(objID: string, data?: sail.SailGameObject, opts: InitGameObjectOpts = {
    save: true,
    select: true,
}): Promise<cc.Node | null> {
    const { save, select } = Object.assign({ save: true, select: true }, opts);
    // 先找到这个对象的数据
    const asset: IAssetObject | null = assetsStore().getAsset(objID);
    if (asset) {
        const opts = data || cloneDeep({
            originObjectID: objID,
            bundle: toRaw(asset.data.bundle),
            behaviors: toRaw(asset.data.behaviors) || [],
            dependencies: {},
        });
        
        const node: cc.Node | null = await sail.createGameObject(objID, opts);
        if (node) {
            sceneAssistant.addGameNode(node, save);
            if (select) {
                sceneAssistant.select(node);
            }
            return node;
        }
    }
    return null;
}

export function getNextOperation(data: ListItem): void {
    (async () => {
        const info = assetsStore().addObject(data.data);
        if (info) {
            await instanceGameObject(info.id);
        }
    })();
    // if (type === SObjectType.Text) {
    //     // 选择位置后，直接添加到scene
    // } else if (type === SObjectType.Sprite) {
    //     // 选择位置后，添加资源文件
    // } else if (type === SObjectType.Touch) {
    //     // 直接加入到对象列表中
    // }
}