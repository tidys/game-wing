import { AceData } from './types';

const AceVar: AceData = {
    name: '判断实例布尔值',
    group: '实例变量',
    type: 'judge-bool',
    desc: '判断实例对象的布尔值变量是否为真',
    asset: 'variable',
};

export const ACE: AceData[] = [AceVar];

