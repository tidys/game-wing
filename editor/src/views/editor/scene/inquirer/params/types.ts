export interface AceData {
    icon?: string,
    name: string,
    group: string,
    type: string,
    desc: string,
    asset: string, // ace关联的对象类型
}
