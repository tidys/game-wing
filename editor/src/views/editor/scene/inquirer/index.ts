import { DialogOptions, Msg } from '@/ui-kit/dialog';
import CreateObject from './crate-object/create-object.vue';
import { Emitter } from '@/ui-kit/index';
import PluginMgr from './plugin-mgr/plugin-mgr.vue';
import AddBehavior from './add-behavior/add-behavior.vue';
import AddVariable from './add-variable/add-var.vue';
import AddCondition from './add-condition/add-condition.vue';
import Conditions from './conditions/index.vue';
import Params from './params/params.vue';
import Action from './action/action.vue';

export function showCreateObject(): void {
    const opts: DialogOptions = {
        title: '创建新对象类型',
        height: 500,
        width: 600,
        comp: CreateObject,
    };
    Emitter.emit(Msg.ShowDialog, opts);
}

export function showPlugMgr(): void {
    const opts: DialogOptions = {
        title: '插件管理',
        height: 500,
        width: 600,
        comp: PluginMgr,
    };
    Emitter.emit(Msg.ShowDialog, opts);
}

export function showBehavior(objectID: string): void {
    const opts: DialogOptions = {
        title: '添加行为',
        width: 600,
        height: 500,
        comp: AddBehavior,
        data: objectID,
    };
    Emitter.emit(Msg.ShowDialog, opts);
}

export function showVar(dialogOptions: DialogOptions, cb: (data: sail.Variable) => void): void {
    const opts: DialogOptions = Object.assign({
        title: '',
        width: 600,
        height: 300,
        comp: AddVariable,
        responseCB: cb,
    }, dialogOptions);
    Emitter.emit(Msg.ShowDialog, opts);
}

export function showConditions(bundle: string, filter: string[], cb: any): void {
    const opts: DialogOptions = {
        title: '添加条件',
        width: 600,
        height: 300,
        data: { bundle, filter },
        comp: Conditions,
        responseCB: cb,
    };
    Emitter.emit(Msg.ShowDialog, opts);
}

export function showActions(bundle: string, filter: string[], cb: any): void {
    const opts: DialogOptions = {
        title: '添加动作',
        width: 600,
        height: 300,
        data: { bundle, filter },
        comp: Action,
        responseCB: cb,
    };
    Emitter.emit(Msg.ShowDialog, opts);
}

export function showConditionDependObject(cb: any): void {
    const opts: DialogOptions = {
        title: '添加条件',
        width: 600,
        height: 300,
        comp: AddCondition,
        responseCB: cb,
    };
    Emitter.emit(Msg.ShowDialog, opts);
}

export function showParams(params: sail.ACEParam[], cb: any): void {
    const opts: DialogOptions = {
        title: '设置参数',
        width: 600,
        height: 300,
        comp: Params,
        responseCB: cb,
        data: params,
    };
    Emitter.emit(Msg.ShowDialog, opts);
}
