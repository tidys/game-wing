import { loadScript } from '../util';
import { boot } from './boot';
import pluginsStore from '../../store/plugins';

export class Engine {
    constructor() {
    }

    init = false;

    async load(): Promise<void> {
        await loadScript('./src/settings.js');
        await loadScript('./cocos2d-js-for-preview.js');
        await this.boot();
        console.log('engine loaded.');
        await pluginsStore().init();
        console.log('plugins loaded.');
        this.init = true;
    }

    async boot(): Promise<void> {
        return new Promise((resolve, reject) => {
            boot(() => {
                resolve();
            });
        });
    }
}

const engine = new Engine();
export default engine;