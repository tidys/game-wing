export function boot(callback: Function): void {
    cc.macro.ENABLE_TRANSPARENT_CANVAS = true;
    cc.macro.CLEANUP_IMAGE_CACHE = true;

    // @ts-ignore
    const settings: any = window._CCSettings;
    const RESOURCES: any = cc.AssetManager.BuiltinBundleName.RESOURCES;
    const INTERNAL: any = cc.AssetManager.BuiltinBundleName.INTERNAL;
    const MAIN: any = cc.AssetManager.BuiltinBundleName.MAIN;


    const option = {
        id: 'GameCanvas',
        debugMode: settings.debug ? cc.debug.DebugMode.INFO : cc.debug.DebugMode.ERROR,
        showFPS: settings.debug,
        frameRate: 60,
        groupList: settings.groupList,
        collisionMatrix: settings.collisionMatrix,
    };
    cc.assetManager.init({
        bundleVers: settings.bundleVers,
        remoteBundles: settings.remoteBundles,
        server: settings.server,
    });

    const bundleRoot: string[] = [INTERNAL];
    settings.hasResourcesBundle && bundleRoot.push(RESOURCES);

    let count = 0;

    function onStart() {
        cc.debug.setDisplayStats(false);
        cc.view.enableRetina(true);
        cc.view.resizeWithBrowserSize(true);
        callback && callback();
    }

    function cb(err: any) {
        if (err) return console.error(err.message, err.stack);
        count++;
        if (count === bundleRoot.length + 1) {
            cc.assetManager.loadBundle(MAIN, function (err: any, bundle) {
                if (!err) cc.game.run(option, onStart);
            });
        }
    }

    cc.assetManager.loadScript(settings.jsList.map(function (x: any) {
        return 'src/' + x;
    }), cb);

    for (let i = 0; i < bundleRoot.length; i++) {
        cc.assetManager.loadBundle(bundleRoot[i], cb);
    }
}
