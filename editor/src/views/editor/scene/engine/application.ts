function topLevelImport(url: string) {
    return import(url);
}

export async function createApplication(para: Record<string, any>) {
    const {loadJsListFile, fetchWasm} = para;
    let promise = Promise.resolve();
    promise = promise.then(() => topLevelImport('wait-for-ammo-instantiation'))
        .then((dd) => {
            console.log(dd);
        });


}

export function start(para: Record<string, any>) {

}
