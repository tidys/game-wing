import { IUiMenuItem } from '@/ui-kit/menu';
import { sceneAssistant } from './index';
import { Emitter } from '@/ui-kit/index';
import { Msg, DialogOptions } from '@/ui-kit/dialog';
import { markRaw, defineAsyncComponent, defineComponent } from 'vue';
import CreateObject from './inquirer/crate-object/create-object.vue';
import { showCreateObject } from './inquirer';

const Menus: IUiMenuItem[] = [
    {
        name: '新建对象',
        callback: () => {
            showCreateObject();
            // const obj = new cc.Node("object");
            // const label: cc.Label = obj.addComponent(cc.Label);
            // label.string = "3134242424";
            // sceneAssistant.addGameNode(obj);
        },
    },
    {
        name: '创建文本',
        enabled: false,
        callback: () => {
            const node = new cc.Node('label');
            const label: cc.Label = node.addComponent(cc.Label);
            label.string = '文本';
            sceneAssistant.addGameNode(node);
        },
    },
    {
        name: '创建graphics',
        enabled: false,
        callback: () => {
            const node = new cc.Node('graphics');
            sceneAssistant.addGameNode(node);
            const g = node.addComponent(cc.Graphics);
            if (g) {
                g.fillColor = cc.Color.RED;
                g.rect(0, 0, 100, 100);
                g.fillRect(0, 0, 100, 100);
                g.stroke();
                g.fill();
            }
        },
    },
    {
        name: '创建图片',
        enabled: false,
        callback: () => {
            const uuid = '0275e94c-56a7-410f-bd1a-fc7483f7d14a';
            cc.assetManager.loadAny({ uuid }, (error, texture) => {
                const node: cc.Node = new cc.Node('sprite');
                sceneAssistant.addGameNode(node);
                const sprite: cc.Sprite = node.addComponent(cc.Sprite);
                sprite.spriteFrame = new cc.SpriteFrame(texture);
                sprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
                node.setContentSize(400, 400);
            });
        },
    },
];

export default {
    Menus,
};
