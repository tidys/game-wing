import { sceneAssistant } from '../index';
import { Point } from './point';


export class Points {
    private leftTop: Point | null = null;
    private leftCenter: Point | null = null;
    private leftBottom: Point | null = null;
    private centerTop: Point | null = null;
    private centerBottom: Point | null = null;
    private rightTop: Point | null = null;
    private rightCenter: Point | null = null;
    private rightBottom: Point | null = null;

    constructor() {
    }

    private getDeltaX(event: cc.Event.EventTouch) {
        return event.getDeltaX() / sceneAssistant.scale;
    }

    private getDeltaY(event: cc.Event.EventTouch) {
        return event.getDeltaY() / sceneAssistant.scale;
    }

    init(parent: cc.Node) {
        this.leftTop = new Point(parent, 'left-top', (event: cc.Event.EventTouch) => {
            if (sceneAssistant.selectNode) {
                sceneAssistant.selectNode.width -= this.getDeltaX(event) * 2;
                sceneAssistant.selectNode.height += this.getDeltaY(event) * 2;
            }
        });
        this.leftCenter = new Point(parent, 'left-center', (event: cc.Event.EventTouch) => {
            if (sceneAssistant.selectNode) {
                sceneAssistant.selectNode.width -= this.getDeltaX(event) * 2;
            }
        });
        this.leftBottom = new Point(parent, 'left-bottom', (event: cc.Event.EventTouch) => {
            if (sceneAssistant.selectNode) {
                sceneAssistant.selectNode.width -= this.getDeltaX(event) * 2;
                sceneAssistant.selectNode.height -= this.getDeltaY(event) * 2;
            }
        });
        this.centerTop = new Point(parent, 'center-top', (event: cc.Event.EventTouch) => {
            if (sceneAssistant.selectNode) {
                sceneAssistant.selectNode.height += this.getDeltaY(event) * 2;
            }
        });
        this.centerBottom = new Point(parent, 'center-bottom', (event: cc.Event.EventTouch) => {
            if (sceneAssistant.selectNode) {
                sceneAssistant.selectNode.height -= this.getDeltaY(event) * 2;
            }
        });
        this.rightTop = new Point(parent, 'right-top', (event: cc.Event.EventTouch) => {
            if (sceneAssistant.selectNode) {
                sceneAssistant.selectNode.width += this.getDeltaX(event) * 2;
                sceneAssistant.selectNode.height += this.getDeltaY(event) * 2;
            }
        });
        this.rightCenter = new Point(parent, 'right-center', (event: cc.Event.EventTouch) => {
            if (sceneAssistant.selectNode) {
                sceneAssistant.selectNode.width += this.getDeltaX(event) * 2;
            }
        });
        this.rightBottom = new Point(parent, 'right-bottom', (event: cc.Event.EventTouch) => {
            if (sceneAssistant.selectNode) {
                sceneAssistant.selectNode.width += this.getDeltaX(event) * 2;
                sceneAssistant.selectNode.height -= this.getDeltaY(event) * 2;
            }
        });
        this._visibleNodes(false);
    }

    get points() {
        return [
            this.leftTop, this.leftCenter, this.leftBottom,
            this.centerTop, this.centerBottom,
            this.rightTop, this.rightCenter, this.rightBottom,
        ];
    }

    private _visibleNodes(b: boolean) {
        this.points.forEach(node => {
            node?.visibleNode(b);
        });
    }

    clean() {
        this._visibleNodes(false);
    }

    update(width: number, height: number) {
        this._visibleNodes(true);
        const halfWidth = width / 2; const halfHeight = height / 2;

        const pointLeftTop = cc.v2(-halfWidth, halfHeight);
        const pointLeftCenter = cc.v2(-halfWidth, 0);
        const pointLeftBottom = cc.v2(-halfWidth, -halfHeight);
        const pointCenterTop = cc.v2(0, halfHeight);
        const pointCenterBottom = cc.v2(0, -halfHeight);
        const pointRightTop = cc.v2(halfWidth, halfHeight);
        const pointRightCenter = cc.v2(halfWidth, 0);
        const pointRightBottom = cc.v2(halfWidth, -halfHeight);

        this.leftTop?.update(pointLeftTop);
        this.leftCenter?.update(pointLeftCenter);
        this.leftBottom?.update(pointLeftBottom);
        this.centerTop?.update(pointCenterTop);
        this.centerBottom?.update(pointCenterBottom);
        this.rightTop?.update(pointRightTop);
        this.rightCenter?.update(pointRightCenter);
        this.rightBottom?.update(pointRightBottom);
    }
}
