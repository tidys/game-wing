import { sceneAssistant } from '../index';

export class Rect {
    private graphicsComp: cc.Graphics | null = null;
    private borderLineColor: cc.Color = cc.Color.RED;
    private press = false;

    init(options: { moveCallback: Function }):any {
        const {moveCallback} = options;
        const node = new cc.Node('gizmo-rect');
        this.graphicsComp = node.addComponent(cc.Graphics);
        node.on(cc.Node.EventType.TOUCH_START, () => {

        });
        node.on(cc.Node.EventType.TOUCH_MOVE, (event: cc.Event.EventTouch) => {
            moveCallback && moveCallback(event);
        });
        node.on(cc.Node.EventType.MOUSE_DOWN, (event: cc.Event.EventMouse) => {
            this.press = true;
        });
        node.on(cc.Node.EventType.MOUSE_MOVE, (event: cc.Event.EventMouse) => {
            if (this.press) {
            }
        });
        node.on(cc.Node.EventType.MOUSE_UP, (event: cc.Event.EventMouse) => {
            this.press = false;
        });
        return node;
    }

    clean() {
        this.graphicsComp?.clear();
        const node = this.graphicsComp?.node;
        if (node) {
            node.active = false;
        }
    }

    update(width: number, height: number) {
        this.graphicsComp?.node.setContentSize(width, height);
        const comp: cc.Graphics | null = this.graphicsComp;
        if (comp) {
            comp.node.active = true;
            comp.clear();
            comp.strokeColor = this.borderLineColor;
            comp.fillColor = cc.color(0, 0, 0, 0);
            comp.lineWidth = 3 / sceneAssistant.scale;
            comp.rect(-width / 2, -height / 2, width, height);

            // 画线有问题，首尾连不上
            // const halfWidth = width / 2, halfHeight = height / 2;
            // const point1 = cc.v2(-halfHeight, -halfHeight);// 左下
            // const point2 = cc.v2(-halfWidth, halfHeight);// 左上
            // const point3 = cc.v2(halfWidth, halfHeight);// 右上
            // const point4 = cc.v2(halfWidth, -halfHeight);// 右下
            // comp.moveTo(point1.x, point1.y);
            // comp.lineTo(point2.x, point2.y);
            // comp.lineTo(point3.x, point3.y);
            // comp.lineTo(point4.x, point4.y);
            // comp.close();

            comp.fill();
            comp.stroke();
        }
    }
}
