export function calcNextIndex(arr: string[]): number {
    // 过滤出数组中的所有数字,并转化为数字
    let array: number[] = arr.map(function (item) {
        const num = item.toString().replace(/[^0-9]/gi, '');
        return parseInt(num);
    });
    // 过滤重复值
    array = array.filter(function (item, index, self) {
        return self.indexOf(item) === index;
    });
    // 排序
    array.sort(function (a, b) {
        return a - b;
    });
    // 去掉为0的开头
    const began = array[0];
    if (began !== undefined && began === 0) {
        array.splice(0, 1);
    }
    
    // 找出空缺的值
    let ret = 1;
    for (let index = 0; index < array.length;) {
        if (array[index] !== ret) {
            break;
        } else {
            ret++;
            index++;
        }
    }
    // 将返回值补充为2位
    return ret;
}