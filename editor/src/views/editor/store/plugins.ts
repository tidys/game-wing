import { defineStore } from 'pinia';
import { toRaw } from 'vue';

const BuiltinObject = ['text', 'system'];
const BuiltinBehaviors = ['fade'];

export interface PluginsStoreState {
    allObjects: sail.GameObject [];
    allBehaviors: sail.GameBehavior[];
}

const TypeGameBehavior = 'behavior';
export const TypeGameObject = 'object';
const pluginsStore = defineStore('plugins', {
    state: (): PluginsStoreState => {
        return {
            allObjects: [],
            allBehaviors: [],
        };
    },
    actions: {
        async init() {
            this.allObjects = [];
            this.allBehaviors = [];
            const allBundle = BuiltinObject.concat(BuiltinBehaviors);
            // 加载bundle
            for (let i = 0; i < allBundle.length; i++) {
                const bundle = allBundle[i];
                console.log(`bundle(${bundle}) loading ...`);
                const ret = await sail.loadBundle(bundle);
                console.log(`bundle(${bundle}) loaded `);
                if (ret) {
                    const { type } = ret;
                    if (type === TypeGameObject) {
                        this._addPlugin(this.allObjects as Array<sail.GameObject>, ret as sail.GameObject);
                    } else if (type === TypeGameBehavior) {
                        this._addPlugin(this.allBehaviors as Array<sail.GameBehavior>, ret as sail.GameBehavior);
                    }
                }
            }
        },
        _addPlugin(array: Array<sail.GameObject | sail.GameBehavior>, item: sail.GameObject | sail.GameBehavior) {
            if (array.find(el => el.bundle === item.bundle)) {
                console.warn(`重复的bundle: ${item.bundle}`);
            } else {
                array.push(item);
            }
        },
        getObjectByID(id: string): sail.GameObject | null {
            return (this.allObjects as Array<sail.GameObject>).find(el => el.bundle === id) || null;
        },
        getBehaviorByID(id: string): sail.GameBehavior | null {
            return (this.allBehaviors as Array<sail.GameBehavior>).find(el => el.bundle === id) || null;
        },
        getCannotInstantiateGameObject(): sail.GameObject[] {
            const ret: sail.GameObject[] = [];
            this.allObjects.forEach(item => {
                if (item.type === TypeGameObject && item.canInstantiate === false) {
                    ret.push(item);
                }
            });
            return ret;
        },
        // 从指定的bundle中查找条件
        getValidConditions(bundle: string, filter: string[]): sail.ConditionData[] {
            const ret: sail.ConditionData[] = [];
            const obj: sail.GameObject | null = this.allObjects.find(el => el.bundle === bundle) || null;
            if (obj) {
                obj.ace?.conditions?.forEach(condition => {
                    if (!filter.find(el => el === condition.id)) {
                        ret.push(toRaw(condition));
                    }
                });
            }
            return ret;
        },
        getActions(bundle: string, filter: string[]) {
            const ret: sail.ConditionData[] = [];
            const obj: sail.GameObject | null = this.allObjects.find(el => el.bundle === bundle) || null;
            if (obj) {
                obj.ace?.actions?.forEach(action => {
                    if (!filter.find(el => el === action.id)) {
                        ret.push(toRaw(action));
                    }
                });
            }
            return ret;
        },
        getConditionByID(bundleID: string, conditionID: string): sail.ConditionData | null {
            const arr = this.getValidConditions(bundleID, []);
            return arr.find(el => el.id === conditionID) || null;
        },
    },
});
export default pluginsStore;
