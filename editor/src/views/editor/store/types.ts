export enum AssetType {
    Scene = 'scene',
    Event = 'event',
    Object = 'object',
}

export interface IAssetInfo {
    id: string;
    name: string;
    icon?: string;
    type: AssetType;
    data: any;
}

export interface IAssetObject extends IAssetInfo {
    data: {
        bundle: string,
        vars: sail.Variable[],
        behaviors: string[],
    };
}