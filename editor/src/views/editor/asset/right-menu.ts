import { IUiMenuItem } from '@/ui-kit/menu';
import { Emitter } from '@/ui-kit/index';
import { showCreateObject } from '../scene/inquirer';
import assetsStore from '../store/assets';
import { IAssetInfo } from '../store/types';
import { Msg } from '../msg';
import * as TreeIndex from '@/ui-kit/tree/index';

function RenameMenu(item: IAssetInfo) {
    return {
        name: '重命名', callback: () => {
            Emitter.emit(TreeIndex.Msg.RenameItem, item);
        },
    };
}

export function buildSceneRootMenu(): IUiMenuItem[] {
    return [
        {
            name: '添加场景',
            callback: () => {
                assetsStore().addScene();
            },
        },
        {
            name: '复制',
            callback: () => {
            
            },
        },
    ];
}

export function buildSceneItemMenu(item: IAssetInfo): IUiMenuItem[] {
    return [
        {
            name: '打开', callback: () => {
                Emitter.emit(Msg.OpenAsset, item);
            },
        },
        RenameMenu(item),
        {
            name: '删除', callback: () => {
                assetsStore().removeScene(item.id);
            },
        },
        {
            name: '编辑事件表', callback: () => {
            
            },
        },
        {
            name: '预览场景', callback: () => {
            
            },
        },
    ];
}


export function buildEventMenu(): IUiMenuItem[] {
    const ret: IUiMenuItem[] = [];
    ret.push({
        name: '添加事件表',
        callback: () => {
            assetsStore().addEvent();
        },
    });
    return ret;
}

export function buildEventItemMenu(item: IAssetInfo): IUiMenuItem[] {
    return [
        {
            name: '打开', callback: () => {
                Emitter.emit(Msg.OpenAsset, item);
            },
        },
        RenameMenu(item),
        {
            name: '删除', callback: () => {
                assetsStore().removeEvent(item.id);
            },
        },
    ];
}

export function buildObjectMenu(): IUiMenuItem[] {
    return [
        {
            name: '创建新的对象', callback: () => {
                showCreateObject();
            },
        },
    ];
}

export function buildObjectItemMenu(item: IAssetInfo): IUiMenuItem[] {
    return [
        {
            name: '打印ID', callback: () => {
                console.log(`${item.name}:${item.id}`);
            },
        },
        RenameMenu(item),
        {
            name: '删除', callback: () => {
                // todo 删除的时候，要查询关联的场景事件
                assetsStore().removeObject(item.id);
            },
        },
    ];
}