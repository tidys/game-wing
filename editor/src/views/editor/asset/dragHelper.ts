export interface DragCreateObjectData {
    id: string;
}

export const DragType = {
    CreateObject: 'CreateObject',
};
