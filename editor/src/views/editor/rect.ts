export class Rect {
  public x = 0;
  public y = 0;
  public width = 0;
  public height = 0;

  constructor(x: number, y: number, width: number, height: number) {
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
  }

  contain(x: number, y: number):boolean {
      if (
          this.x < x &&
      x < this.x + this.width &&
      this.y < y &&
      y < this.y + this.height
      ) {
          return true;
      }
      return false;
  }
}
