import { defineStore } from 'pinia';
import { ProjectList } from './tyeps';

export interface IDashboardStore {
    lists: ProjectList[];
}

const dashboardStore = defineStore('dashboard', {
    state: (): IDashboardStore => {
        return {
            lists: [],
        };
    },
    actions: {
        setValue(id: string) {
            this.lists.push({
                id,
                name: `test-${id}`,
            });
        },
    },
});
export default dashboardStore;