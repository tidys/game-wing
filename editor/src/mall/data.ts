export interface BundleInfo {
    name: string;
    url: string;
    desc:string;
}