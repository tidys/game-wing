#! /usr/bin/env node

const FsExtra = require('fs-extra');
const { program } = require('commander');
const Path = require('path');
const OsEnv = require('osenv');
const filePath = Path.join(OsEnv.home(), 'sail-cli.json');

if (!FsExtra.pathExistsSync(filePath)) {
    FsExtra.writeJsonSync(filePath, {});
}

const data = FsExtra.readJsonSync(filePath);
const { iconFontDir } = data;


const curDir = process.cwd();
program.command('if-set').action(() => {
    data.iconFontDir = curDir;
    console.log(filePath);
    FsExtra.writeJsonSync(filePath, data);
});
program.command('if-update').action(() => {
    //  检查是不是iconfont的目录
    const files = ['demo.css', 'iconfont.css', 'iconfont.js', 'iconfont.json', 'iconfont.ttf'];

    const missFile = files.find(el => {
        const file = Path.join(curDir, el);
        return !FsExtra.pathExistsSync(file);
    });
    if (missFile) {
        console.log(`无法更新iconfont，缺失文件：${missFile}`);
        return;
    }

    if (iconFontDir && FsExtra.pathExistsSync(iconFontDir)) {
        FsExtra.emptydirSync(iconFontDir);
        FsExtra.copySync(curDir, iconFontDir);
        console.log('更新iconfont成功！');
        console.log(curDir);
        // FsExtra.removeSync(curDir);
        FsExtra.remove(curDir);
    } else {
        console.log('未发现iconfont目录！');
    }
});
program.parse(process.argv);
